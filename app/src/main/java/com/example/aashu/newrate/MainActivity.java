package com.example.aashu.newrate;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {


    Spinner sp;
    String value[];
    Button b1;
    TextView t1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        sp=findViewById(R.id.spinner1);
        b1=findViewById(R.id.button1);
        t1=findViewById(R.id.textview1);
        EditText forwardText=findViewById(R.id.forwardText);
        String s=((Button)findViewById(R.id.textbtn)).getText().toString();
        forwardText.setText(s);
        String s2= String.valueOf(forwardText.getText());

        ((TextView)findViewById(R.id.tv)).setText(s2);


        findViewById(R.id.menubtn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                startActivity(new Intent(MainActivity.this,Menu_Activity.class));

            }
        });


        value=getResources().getStringArray(R.array.data);

        ArrayAdapter<String> arrayAdapter=new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1,value);

        sp.setAdapter(arrayAdapter);

        b1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String item=sp.getSelectedItem().toString();

//                Toast.makeText(MainActivity.this, ""+item, Toast.LENGTH_SHORT).show();

                t1.setText(item);


            }
        });




    }
}
