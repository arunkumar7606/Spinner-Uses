package com.example.aashu.newrate;

import android.content.Intent;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;

public class Menu_Activity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_);




    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {


//
//        MenuInflater inflater=getMenuInflater();
//        inflater.inflate(R.menu.options_menu,menu);
        getMenuInflater().inflate(R.menu.options_menu,menu);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id=item.getItemId();
        switch (id){
            case R.id.item1:
                Toast.makeText(this, "welcome to home page", Toast.LENGTH_SHORT).show();
                break;
            case R.id.item2 :
                Toast.makeText(this, "welcome to About", Toast.LENGTH_SHORT).show();
                 break;
            case R.id.item3:
                startActivity(new Intent(Settings.ACTION_SETTINGS));

                break;
        }

//        Toast.makeText(this, "kaam ho gya", Toast.LENGTH_SHORT).show();
        return super.onOptionsItemSelected(item);
    }
}
